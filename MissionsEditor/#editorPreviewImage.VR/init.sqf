TEST_IncludedAuthors = ["[TF]Nkey & TurkishSoap","ACE Team","Anitsoy","AWAR & [TF]Nkey & TurkishSoap","AWAR & IFA3 Team","AWAR & Joarius","AWAR & Jove Chiere","AWAR & Lawz","AWAR & Lennard","AWAR & Shvetz","AWAR & Snippers","AWAR & Stadl0r","AWAR & swurvin","AWAR","Bax & Jeg0r","Bax","Bohemia Interactive & Lennard","Bohemia Interactive","Bricks","Crotmoul","CSA38 & IFA3 Team","CSA38","DontShootMe!! & swurvin","DontShootMe!!","El Tyranos","GSTAVO","IFA3 Team & Lennard","IFA3 Team","iron_excelsior","Jaki","Jeg0r & Hicks","Joarius","Jove Chiere","Kerc Kasha","Kutejnikov","Lawz","LAxemann","LEN","Lennard","Major Boogie","Reyhard & Joarius","RJW","Rylan Young","SenChi","Snippers","Sokolonko & IFA3 Team","Stadl0r","Stagler","swurvin","Taro","tierprot & [TF]Nkey & TurkishSoap","V!nc3r"];

TEST_IncludedAuthors = ["","AWAR & I44 Team","I44 Team & IFA3 Team","I44 Team","McNools & swurvin","McNools","NSU | Macolik","Macolik & swurvin"];

///////////////////////////////////////////////////////////////////////////////

Test_doConfigExport = false;
//Test_doConfigExport = true;

Test_createMissingOnly = false;
Test_createMissingOnly = true;

///////////////////////////////////////////////////////////////////////////////

Test_exportType = "vehicles";
Test_exportType = "props";
Test_exportType = "all";

///////////////////////////////////////////////////////////////////////////////

LIB_System_Tanks_f_AntiFlip_Active = false;

Test_doListedClassesOnly =
[

];

Test_restrictedModels =
[
//	"\A3\Weapons_f\dummyweapon.p3d",
	"\A3\Weapons_f\laserTgt.p3d",
	"\A3\Structures_F\Mil\Helipads\HelipadEmpty_F.p3d",
	""
];
Test_blacklistClassTree =
[
//	"CAManBase",
//	"WeaponHolder",
//	"Bag_Base",
	"LaserTarget"
];
Test_whiteListClassTree =
[
	"Bag_Base"
];

Test_dummyEditorPreviewImages =
[
	"\a3\editorpreviews_f\data\cfgvehicles\default\armor.jpg",
	"\a3\editorpreviews_f\data\cfgvehicles\default\car.jpg",
	"\a3\editorpreviews_f\data\cfgvehicles\default\man.jpg",
	"\a3\editorpreviews_f\data\cfgvehicles\default\prop.jpg",
	"\a3\editorpreviews_f\data\cfgvehicles\default\ship.jpg"
];

///////////////////////////////////////////////////////////////////////////////

TEST_IncludedAuthors = TEST_IncludedAuthors apply {toLower _x};

if (Test_createMissingOnly) then
{
	_dummy = preprocessFile "missing.sqm";
	_dummy = execVM "missing.sqm";
};

_handle = [nil,Test_exportType] execVM "fn_exportEditorPreviews.sqf";

waitUntil {scriptDone _handle};

endLoadingScreen;

endMission "END1";

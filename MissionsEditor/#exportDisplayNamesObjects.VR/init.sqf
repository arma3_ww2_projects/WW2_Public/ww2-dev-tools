TEST_IncludedVehicleTypes = [];

startLoadingScreen ["WAIT",""];
Test_Vehicles = [] call compile preprocessFileLineNumbers "createVehicleList.sqf";

TEST_UniqueModels = [];

_export = "";

diag_log "START";

{
	private _vehicleClass = _x;
	private _model = toLower (getText (configFile/"CfgVehicles"/_vehicleClass/"model"));

	if (!(_model in TEST_UniqueModels)) then
	{
//		TEST_UniqueModels pushBack _model;

		_newString = format ["	class %1: %2",_vehicleClass,configName (inheritsFrom (configFile/"CfgVehicles"/_vehicleClass))] + endl;
		_newString = _newString + format ['	{',""] + endl;
		_newString = _newString + format ['		displayName = "%1";',getText (configFile/"CfgVehicles"/_vehicleClass/"displayName")] + endl;
		_newString = _newString + format ['		model = "%1";',getText (configFile/"CfgVehicles"/_vehicleClass/"model")] + endl;
		_newString = _newString + format ['	};',""] + endl;

		_export = _export + _newString;
	};
} forEach Test_Vehicles;

copyToClipboard _export;

endLoadingScreen;

endMission "END1";
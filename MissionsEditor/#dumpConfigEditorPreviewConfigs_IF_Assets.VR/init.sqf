TEST_IncludedAuthors = ["[TF]Nkey & TurkishSoap","ACE Team","Anitsoy","AWAR & [TF]Nkey & TurkishSoap","AWAR & IFA3 Team","AWAR & Joarius","AWAR & Jove Chiere","AWAR & Lawz","AWAR & Lennard","AWAR & Shvetz","AWAR & Snippers","AWAR & Stadl0r","AWAR & swurvin","AWAR","Bax & Jeg0r","Bax","Bohemia Interactive & Lennard","Bohemia Interactive","Bricks","Crotmoul","CSA38 & IFA3 Team","CSA38","DontShootMe!! & swurvin","DontShootMe!!","El Tyranos","GSTAVO","IFA3 Team & Lennard","IFA3 Team","iron_excelsior","Jaki","Jeg0r & Hicks","Joarius","Jove Chiere","Kerc Kasha","Kutejnikov","Lawz","LAxemann","LEN","Lennard","Major Boogie","Reyhard & Joarius","RJW","Rylan Young","SenChi","Snippers","Sokolonko & IFA3 Team","Stadl0r","Stagler","swurvin","Taro","tierprot & [TF]Nkey & TurkishSoap","V!nc3r"];

TEST_IncludedAuthors = TEST_IncludedAuthors apply {toLower _x};

_path = "\WW2\Core_t\IF_EditorPreviews_t\";

startLoadingScreen [""];

"ConfigDumpFileIO" callExtension ("open:" + "A3_EditorPreviewConfigs_IF_Assets.cpp");

_rootClass = "CfgVehicles";

for "_i" from (0) to ((count(configFile/_rootClass)) - 1) do
{
	private["_class"];
	_class = (configFile/_rootClass) select _i;

	if (isClass _class) then
	{
		private["_scope","_model"];
		_scope = getNumber(_class/"scope");
		_model = getText(_class/"model");

		if ((_scope == 2) && (_model != "")) then
		{
			private["_className"];
			_className = configName _class;

			if (_className isKindOf "AllVehicles") then
			{
				private["_author"];
				_author = toLower (getText (_class/"author"));
				if (_author in TEST_IncludedAuthors) then
				{
					_baseClass = configName (inheritsFrom _class);

					_myString = format ["	class %1: %2",_className, _baseClass];
					"ConfigDumpFileIO" callExtension ("write:" + _myString);

					_myString = "	{";
					"ConfigDumpFileIO" callExtension ("write:" + _myString);

					_myString = format ['		editorPreview = "%1%2.jpg";',_path,_className];
					"ConfigDumpFileIO" callExtension ("write:" + _myString);

					_myString = "	};";
					"ConfigDumpFileIO" callExtension ("write:" + _myString);
				};
			};
		};
	};
};

"ConfigDumpFileIO" callExtension "close:yes";

endLoadingScreen;

endMission "END1";
_rootClass = "CfgVehicles";
_return = [];

for "_i" from (0) to ((count(configFile/_rootClass)) - 1) do
{
	_class = (configFile/_rootClass) select _i;

	if (isClass _class) then
	{
		_scope = getNumber(_class/"scope");
		_model = getText(_class/"model");

		if ((_scope == 1) && (_model != "")) then
		{
			_className = configName _class;
			_displayNameEmpty = ((getText (_class/"displayName")) == "");
			_isObject = true;
//diag_log _className;

			{
				if (_className isKindOf _x) exitWith {_isObject = false;};
			}
			forEach
			[
				"AllVehicles",
				"Logic",
				"LaserTarget",
				"NVTarget",
				"ArtilleryTarget",
				"FireSectorTarget",
				"Rope",
				"WindAnomaly",
				"placed_chemlight_green",
				"NVG_TargetBase",
				"placed_B_IR_grenade",
				"Sound",
				"ThingEffect",
				"ReammoBox",
				"Plane_Canopy_Base_F",
				"Library_WeaponHolder",
				"TargetCenter",
				"Bomb",
				"ReammoBox_F",
				"PlaneWreck",
				"Lightning_F",
				"Lightning1_F",
				"ModuleEmpty_F",
				"Sphere_3DEN",
				"FlagCarrier",
				"MemoryFragment_F"
			];

			if (_isObject) then// && {!(_displayNameEmpty)}) then
			{
				if ((count TEST_IncludedVehicleTypes) > 0) then
				{
					{
						if (_className isKindOf _x) exitWith
						{
							_return pushBack _className;
						};
					} forEach TEST_IncludedVehicleTypes;
				}
				else
				{
					_return pushBack _className;
				};
			};
		};
	};
};
_return;
TEST_IncludedAuthors = ["[TF]Nkey & TurkishSoap","ACE Team","Anitsoy","AWAR & [TF]Nkey & TurkishSoap","AWAR & IFA3 Team","AWAR & Joarius","AWAR & Jove Chiere","AWAR & Lawz","AWAR & Lennard","AWAR & Shvetz","AWAR & Snippers","AWAR & Stadl0r","AWAR & swurvin","AWAR","Bax & Jeg0r","Bax","Bohemia Interactive & Lennard","Bohemia Interactive","Bricks","Crotmoul","CSA38 & IFA3 Team","CSA38","DontShootMe!! & swurvin","DontShootMe!!","El Tyranos","GSTAVO","IFA3 Team & Lennard","IFA3 Team","iron_excelsior","Jaki","Jeg0r & Hicks","Joarius","Jove Chiere","Kerc Kasha","Kutejnikov","Lawz","LAxemann","LEN","Lennard","Major Boogie","Reyhard & Joarius","RJW","Rylan Young","SenChi","Snippers","Sokolonko & IFA3 Team","Stadl0r","Stagler","swurvin","Taro","tierprot & [TF]Nkey & TurkishSoap","V!nc3r"];

TEST_IncludedAuthors = ["","AWAR & I44 Team","I44 Team & IFA3 Team","I44 Team","McNools & swurvin","McNools","NSU | Macolik","Macolik & swurvin"];

//not in ["Bohemia Interactive","Bravo Zero One Studios","$SAM_System_01_base_F"]

///////////////////////////////////////////////////////////////////////////////

TEST_IncludedAuthors = TEST_IncludedAuthors apply {toLower _x};

startLoadingScreen ["WAIT",""];

Test_fnc_diagMacrosSimpleObject = compile preprocessFileLineNumbers "fn_diagMacrosSimpleObject.sqf";

Test_fnc_adjustSimpleObject = compile preprocessFileLineNumbers "fn_adjustSimpleObject.sqf";
Test_fnc_createSimpleObject = compile preprocessFileLineNumbers "fn_createSimpleObject.sqf";
Test_fnc_replaceWithSimpleObject = compile preprocessFileLineNumbers "fn_replaceWithSimpleObject.sqf";
Test_fnc_simpleObjectData = compile preprocessFileLineNumbers "fn_simpleObjectData.sqf";

"ConfigDumpFileIO" callExtension ("open:" + "A3_SimpleObjects.cpp");

[] call Test_fnc_diagMacrosSimpleObject;

"ConfigDumpFileIO" callExtension "close:yes";

endLoadingScreen;

endMission "END1";

//TEST_IncludedVehicleTypes = [];
//TEST_IncludedFactions = [];
//TEST_IncludeWinterType = false;
TEST_IncludedAuthors = ["[TF]Nkey & TurkishSoap","ACE Team","Anitsoy","AWAR & [TF]Nkey & TurkishSoap","AWAR & IFA3 Team","AWAR & Joarius","AWAR & Jove Chiere","AWAR & Lawz","AWAR & Lennard","AWAR & Shvetz","AWAR & Snippers","AWAR & Stadl0r","AWAR & swurvin","AWAR","Bax & Jeg0r","Bax","Bohemia Interactive & Lennard","Bohemia Interactive","Bricks","Crotmoul","CSA38 & IFA3 Team","CSA38","DontShootMe!! & swurvin","DontShootMe!!","El Tyranos","GSTAVO","IFA3 Team & Lennard","IFA3 Team","iron_excelsior","Jaki","Jeg0r & Hicks","Joarius","Jove Chiere","Kerc Kasha","Kutejnikov","Lawz","LAxemann","LEN","Lennard","Major Boogie","Reyhard & Joarius","RJW","Rylan Young","SenChi","Snippers","Sokolonko & IFA3 Team","Stadl0r","Stagler","swurvin","Taro","tierprot & [TF]Nkey & TurkishSoap","V!nc3r"];

TEST_IncludedAuthors = TEST_IncludedAuthors apply {toLower _x};

startLoadingScreen ["WAIT",""];
MyVehicles = [] call compile preprocessFileLineNumbers "createVehicleList.sqf";

if (true) then
{
	private["_xCoord","_yCoord"];
	_xCoord = (getMarkerPos "target") select 0;
	_yCoord = (getMarkerPos "target") select 1;
	{
		diag_log "";
		diag_log _x;

		_vehicle = createVehicle [_x,[_xCoord,_yCoord,0],[],0,"CAN_COLLIDE"];

		deleteVehicle _vehicle;
	} forEach MyVehicles;
};
endLoadingScreen;

endMission "END1";
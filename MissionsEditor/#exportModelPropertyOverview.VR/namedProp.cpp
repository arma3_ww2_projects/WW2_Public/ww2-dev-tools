<1.8e+016 ShadowVolumeViewCargo>

1201_0

unknown 14999999335104512
unknown 18009999389229056
unknown 8009999653535744

ViewGunner|ViewPilot|ViewCargo|ViewCargoGeom|shadowvolumes|edits|Geometry|Bouyancy|Physx|Memory|LandContact|Roadway|Paths|Hitpoints|ViewGeometry|FireGeometry|ViewCargoGeometry|ViewCargoFireGeometry|ViewCommander|ViewCommanderGeometry|ViewCommanderFireGeometry|ViewPilotGeometry|ViewPilotFireGeometry|ViewGunnerGeometry|ViewGunnerFireGeometry|SubParts|ShadowVolumeViewCargo|ShadowVolumeViewPilot|ShadowVolumeViewGunner|Wreck


lod's accepted name	or, lod's resolution
All	all lods
ViewGunner	1000.0
ViewPilot	1100.0
ViewCargo	1200.0
ViewCargoGeom	1202.0
shadowvolumes	10000.0 ...
edits	20000.0 ...
Geometry	1.0e13
Bouyancy	2.0e13
Physx(old)	3.0e13
Physx	4.0e13
Memory	1.0e15
LandContact	2.0e15
Roadway	3.0e15
Paths	4.0e15
Hitpoints	5.0e15
ViewGeometry	6.0e15
FireGeometry	7.0e15
ViewCargoGeometry	8.0e15
ViewCargoFireGeometry	9.0e15
ViewCommander	1.0e16
ViewCommanderGeometry	1.1e16
ViewCommanderFireGeometry	1.2e16
ViewPilotGeometry	1.3e16
ViewPilotFireGeometry	1.4e16
ViewGunnerGeometry	1.5e16
ViewGunnerFireGeometry	1.6e16
SubParts	1.7e16
ShadowVolumeViewCargo	1.8e16
ShadowVolumeViewPilot	1.9e16
ShadowVolumeViewGunner	2.0e16
Wreck	2.1e16

###

<1000.0 ViewGunner>
<1100.0 ViewPilot>
<1200.0 ViewCargo>
<1e+013 Geometry>
<1e+015 Memory>
<2e+015 LandContact>
<3e+015 Roadway>
<4e+013 Physx>
<4e+015 Paths>
<5e+015 Hitpoints>
<6e+015 ViewGeometry>
<7e+015 FireGeometry>
<8e+015 ViewCargoGeometry>
<9e+015 ViewCargoFireGeometry>

<1.3e+016 ViewPilotGeometry>
<1.5e+016 ViewGunnerGeometry>
<1.8e+016 ShadowVolumeViewCargo>
<1.9e+016 ShadowVolumeViewPilot>

<10000.0 ShadowVolume 0>
<10001.0 ShadowVolume 1>
<10002.0 ShadowVolume 2>
<10003.0 ShadowVolume 3>
<10004.0 ShadowVolume 4>
<10005.0 ShadowVolume 5>
<10009.0 ShadowVolume 9>
<10010.0 ShadowVolume 10>
<10020.0 ShadowVolume 20>
<10100.0 ShadowVolume 100>
<11000.0 ShadowVolume 1000>
<11001.0 ShadowVolume 1001>
<11003.0 ShadowVolume 1003>
<11004.0 ShadowVolume 1004>
<11010.0 ShadowVolume 1010>
<11020.0 ShadowVolume 1020>
<11030.0 ShadowVolume 1030>
<11100.0 ShadowVolume 1100>
<11500.0 ShadowVolume 1500>

<0.0>
<0.083>
<0.1>
<0.15>
<0.2>
<0.25>
<0.3>
<0.35>
<0.4>
<0.5>
<0.6>
<0.7>
<0.75>
<0.8>
<0.8462933>
<1.0>
<1.083>
<1.1>
<1.2>
<1.25>
<1.4>
<1.45>
<1.5>
<1.7>
<10.0>
<10.5>
<100.0>
<11.0>
<12.0>
<12.75>
<14.0>
<15.0>
<16.0>
<2.0>
<2.1>
<2.25>
<2.375>
<2.4>
<2.5>
<2.8>
<20.0>
<21.0>
<21000.0 Edit 1000>
<22.0>
<3.0>
<3.375>
<3.5>
<3.75>
<30.0>
<4.0>
<4.063>
<4.5>
<4.8>
<5.0>
<5.0625>
<5.063>
<5.5>
<5.625>
<6.0>
<6.25>
<6.5>
<6.75>
<7.0>
<7.5>
<7.59375>
<8.0>
<8.1563015>
<8.5>
<8.75>
<9.0>
<9.75>
<9.9>
<900.0>

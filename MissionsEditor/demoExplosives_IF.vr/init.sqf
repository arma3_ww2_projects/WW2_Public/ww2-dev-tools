TEST_IncludedAuthors = ["[TF]Nkey & TurkishSoap","ACE Team","Anitsoy","AWAR & [TF]Nkey & TurkishSoap","AWAR & IFA3 Team","AWAR & Joarius","AWAR & Jove Chiere","AWAR & Lawz","AWAR & Lennard","AWAR & Shvetz","AWAR & Snippers","AWAR & Stadl0r","AWAR & swurvin","AWAR","Bax & Jeg0r","Bax","Bohemia Interactive & Lennard","Bohemia Interactive","Bricks","Crotmoul","CSA38 & IFA3 Team","CSA38","DontShootMe!! & swurvin","DontShootMe!!","El Tyranos","GSTAVO","IFA3 Team & Lennard","IFA3 Team","iron_excelsior","Jaki","Jeg0r & Hicks","Joarius","Jove Chiere","Kerc Kasha","Kutejnikov","Lawz","LAxemann","LEN","Lennard","Major Boogie","Reyhard & Joarius","RJW","Rylan Young","SenChi","Snippers","Sokolonko & IFA3 Team","Stadl0r","Stagler","swurvin","Taro","tierprot & [TF]Nkey & TurkishSoap","V!nc3r"];

TEST_IncludedAuthors = TEST_IncludedAuthors apply {toLower _x};

///////////////////////////////////////////////////////////////////////////////

_spaceBetweenItems = 5;

///////////////////////////////////////////////////////////////////////////////

startLoadingScreen [""];

_rootClass = "CfgVehicles";
_mineClasses = [];

for "_i" from (0) to ((count(configFile/_rootClass)) - 1) do
{
	_class = (configFile/_rootClass) select _i;

	if (isClass _class) then
	{
		_className = configName _class;

		if (_className isKindOf "MineBase") then
		{
			if (getNumber (_class/"scope") > 0) then
			{
				_author = getText (_class >> "author");

				if (toLower _author in TEST_IncludedAuthors) then
				{
					_mineClasses pushBack _className;
				};
			};
		};
	};
};

_numberOfMines = count _mineClasses;
_xMax = 999999;//round (sqrt _numberOfMines);

_positionX = 0;
_positionY = 0;

_x = 5;
_y = 0;

for "_i" from 0 to (_numberOfMines - 1) do
{
	_mineClass = _mineClasses select _i;

	_mine = createMine [_mineClass,[_positionX + _x * _spaceBetweenItems,_positionY + _y * _spaceBetweenItems,0],[],0];
//	_weapon = createVehicle [_mineClass,[_positionX + _x * _spaceBetweenItems,_positionY + _y * _spaceBetweenItems,0],[],0,"CAN_COLLIDE"];

	_x = _x + 1;

	if (_x >= _xMax) then
	{
		_x = 0;
		_y = _y + 1;
	};
};
endLoadingScreen;

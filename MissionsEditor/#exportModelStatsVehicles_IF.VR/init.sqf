TEST_IncludedVehicleTypes = [];
TEST_IncludedFactions = ["LIB_ACI","LIB_ARR","LIB_CIV","LIB_DAK","LIB_FFI","LIB_FSJ","LIB_GUER","LIB_LUFTWAFFE","LIB_LUFTWAFFE_w","LIB_MKHL","LIB_NAC","LIB_NKVD","LIB_PANZERWAFFE","LIB_PANZERWAFFE_w","LIB_RAAF","LIB_RAF","LIB_RBAF","LIB_RKKA","LIB_RKKA_w","LIB_UK_AB","LIB_UK_AB_W","LIB_UK_ARMY","LIB_UK_ARMY_W","LIB_UK_DR","LIB_US_101AB","LIB_US_82AB","LIB_US_AIRFORCE","LIB_US_AIRFORCE_w","LIB_US_ARMY","LIB_US_ARMY_w","LIB_US_RANGERS","LIB_US_TANK_TROOPS","LIB_US_TANK_TROOPS_w","LIB_USSR_AIRFORCE","LIB_USSR_AIRFORCE_w","LIB_USSR_TANK_TROOPS","LIB_USSR_TANK_TROOPS_w","LIB_WEHRMACHT","LIB_WEHRMACHT_w","SG_STURM","SG_STURM_w","SG_STURMPANZER"];
TEST_IncludeWinterType = false;

startLoadingScreen ["WAIT",""];
MyVehicles = [] call compile preprocessFileLineNumbers "createVehicleList.sqf";
endLoadingScreen;

TEST_ModelPath = [];
TEST_ModelStats = [];
TEST_ModelLodsNames = [];

[] spawn
{
	startLoadingScreen ["WAIT",""];

	{
		_className = _x;

		_modelPath = getText (configFile >> "CfgVehicles" >> _className >> "model");

		if ((!(_modelPath in TEST_ModelPath)) && (fileExists _modelPath)) then
		{
			TEST_ModelPath pushBack _modelPath;

//			diag_log "";
//			diag_log _className;
//			diag_log _modelPath;

			_modelResolutionLodsCount = diag_modelResolutionLodsCount _modelPath;
//			waitUntil {_modelResolutionLodsCount = diag_modelResolutionLodsCount _modelPath; (count _modelResolutionLodsCount) >0 };
//			diag_log _modelResolutionLodsCount;

			_modelLodsNames = [];
			waitUntil {_modelLodsNames = diag_modelLodsNames _modelPath; (count _modelLodsNames) >0 };
//			diag_log _modelLodsNames;

			_modelSections = [];
			waitUntil {_modelSections = diag_modelSections _modelPath; (count _modelSections) >0 };
//			diag_log _modelSections;

			_modelFacesCount = [];
			waitUntil {_modelFacesCount = diag_modelFacesCount _modelPath; (count _modelFacesCount) >0 };
//			diag_log _modelFacesCount;

			_modelVerticesCount = [];
			waitUntil {_modelVerticesCount = diag_modelVerticesCount _modelPath; (count _modelVerticesCount) >0 };
//			diag_log _modelVerticesCount;

			_modelStats =
			[
				_className,
				_modelPath,
				_modelResolutionLodsCount,
				_modelLodsNames,
				_modelSections,
				_modelFacesCount,
				_modelVerticesCount
			];

			{TEST_ModelLodsNames pushBackUnique (toLower _x);} forEach _modelLodsNames;

			if ((_modelPath != "") && ((count _modelLodsNames) > 0)) then
			{
				TEST_ModelStats pushBack _modelStats;
			}
			else
			{
				diag_log ["BUG",_className,_modelPath,_modelLodsNames];
			};
		};
	}
	forEach MyVehicles;

	TEST_ModelLodsNames sort true;

	diag_log format ["CLASS	MODEL	RES LODS	SECTIONS	%1	FACES	%1	VERTICES	%1",TEST_ModelLodsNames joinString "	"];

	{
		_modelStats = _x;

		_modelStats call
		{
			params ["_className","_modelPath","_modelResolutionLodsCount","_modelLodsNames","_modelSections","_modelFacesCount","_modelVerticesCount"];
//diag_log ["XXX",_className,_modelPath,_modelResolutionLodsCount,_modelLodsNames,_modelSections,_modelFacesCount,_modelVerticesCount];

			_sections = [];
			_faces = [];
			_vertices = [];

			_pruned = false;

			{
				_lodName = toLower _x;

				_modelLodsNames = _modelLodsNames apply {toLower _x;};

				_index = _modelLodsNames find _lodName;

				_sectionCount = 0;
				_facesCount = 0;
				_verticesCount = 0;

				if (_index != -1) then
				{
					_sectionCount = _modelSections select _index;
					_facesCount = _modelFacesCount select _index;
					_verticesCount = _modelVerticesCount select _index;
				};

				if ((_forEachIndex == 0) && {_index == -1}) then
				{
					_pruned = true;
				}
				else
				{
					_sections pushBack _sectionCount;
					_faces pushBack _facesCount;
					_vertices pushBack _verticesCount;
				};
			}
			forEach TEST_ModelLodsNames;

			if (_pruned) then
			{
				_sections pushBack 0;
				_faces pushBack 0;
				_vertices pushBack 0;
			};

			_return = format ["%1	%2	%3	-	%4	-	%5	-	%6",_className,_modelPath,_modelResolutionLodsCount,_sections joinString "	",_faces joinString "	",_vertices joinString "	"];

			diag_log _return;
		};
	}
	forEach TEST_ModelStats;

	endLoadingScreen;

	endMission "END1";
};

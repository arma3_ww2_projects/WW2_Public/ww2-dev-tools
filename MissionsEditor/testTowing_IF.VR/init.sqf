player allowDamage false;
player addEventHandler ["HandleDamage",{0}];

if (!isServer) exitWith {};

TEST_IncludedVehicleTypes = ["LIB_StaticCannon_base"];
TEST_IncludedFactions = [];
TEST_IncludeWinterType = false;

startLoadingScreen ["WAIT",""];
_staticWeapons = [false] call compile preprocessFileLineNumbers "createVehicleList.sqf";

TEST_IncludedVehicleTypes = ["LIB_Truck_base","LIB_Wheeled_APC_base"];
_trucks = [true] call compile preprocessFileLineNumbers "createVehicleList.sqf";
_wheeled_APCs = [true] call compile preprocessFileLineNumbers "createVehicleList.sqf";

_transports = [];

{
	_transports pushBackUnique _x;
}
forEach (_wheeled_APCs + _trucks);

_fnc_createVehicles =
{
	private _vehicleType = _this select 0;
	private _startPosition = _this select 1;
	private _turnVehicle = _this select 2;

	private _xCoord = _startPosition select 0;
	private _yCoord = _startPosition select 1;

	_vehicle = createVehicle [_vehicleType,[_xCoord,_yCoord,0],[],0,"CAN_COLLIDE"];

	_vehicle allowDamage false;
	_vehicle addEventHandler ["HandleDamage",{0}];

	if (_turnVehicle) then {_vehicle setDir 180;};
};

_yCoord = 0;
{
	_staticWeapon = _x;

	_xCoord = 0;

	{
		_transport = _x;

		[_staticWeapon,[_xCoord,_yCoord],true] call _fnc_createVehicles;
		[_transport,[_xCoord,_yCoord + 10],false] call _fnc_createVehicles;

		_xCoord = _xCoord + 25;
	}
	forEach _transports;

	_yCoord = _yCoord + 50;
}
forEach _staticWeapons;

//[_staticWeapon + _staticWeapon + _staticWeapon + _staticWeapon + _staticWeapon,[10,10],true] call _fnc_createVehicles;
//[_wheeled_APCs + _trucks,[10,40],false] call _fnc_createVehicles;

endLoadingScreen;

set _FROM=2017-02-20
set _FILE=WW2_ChangeLog_Objects_%_FROM%.txt

echo "START" >> %_FILE%

cd Objects_c
echo //Objects_c >> ..\%_FILE%
git log --pretty=format:"%%B" --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Objects_m
REM echo //Objects_m >> ..\%_FILE%
REM git log --pretty=format:"%%B" --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Objects_r
REM echo //Objects_r >> ..\%_FILE%
REM git log --pretty=format:"%%B" --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Objects_t
REM echo //Objects_t >> ..\%_FILE%
REM git log --pretty=format:"%%B" --since==%_FROM% >> ..\%_FILE%
cd ..

rem Prepare
CALL _configureSettings.bat
rem ----

for /D %%i in (TerrainsIF,TerrainsWinterIF) do (

	cd %%i

	for /D %%j in (*_w) do (

		"%_APPPATH%\Makepbo.exe" %_PARAMS_STANDARD% "%_SOURCEPATH%\%%i\%%j" "%_TARGETPATH_DEV%\addons\WW2_%%i_%%j" 1>>"%_SOURCEPATH%\BuildPBOs_%%i.log" 2>>&1

	)

	cd..

)
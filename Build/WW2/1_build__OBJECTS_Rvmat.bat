rem Prepare
CALL _configureSettings.bat
rem ----

for /D %%i in (Objects_r) do (

	cd %%i

	for /D %%j in (*) do (

		cd %%j

		for /D %%k in (*) do (

			"%_APPPATH%\Makepbo.exe" %_PARAMS_STANDARD% "%_SOURCEPATH%\%%i\%%j\%%k" "%_TARGETPATH_DEV%\addons\WW2_%%i_%%j_%%k" 1>>"%_SOURCEPATH%\BuildPBOs_%%i.log" 2>>&1

		)

		cd..

	)

	cd..

)

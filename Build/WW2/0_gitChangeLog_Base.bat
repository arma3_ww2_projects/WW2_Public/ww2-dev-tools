set _FROM=2014-03-06
set _FILE=WW2_ChangeLog_Base_%_FROM%.txt

echo "START" >> %_FILE%

cd Assets_c
echo //Assets_c >> ..\%_FILE%
git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Assets_m
REM echo //Assets_m >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Assets_r
REM echo //Assets_r >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Assets_s
REM echo //Assets_s >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Assets_t
REM echo //Assets_t >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_a
REM echo //Core_a >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_c
REM echo //Core_c >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_f
REM echo //Core_f >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_m
REM echo //Core_m >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_r
REM echo //Core_r >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_s
REM echo //Core_s >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..
REM cd Core_t
REM echo //Core_t >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..

cd AssetsSource_m
echo //AssetsSource_m >> ..\%_FILE%
git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
cd ..
REM cd CoreSource_m
REM echo //CoreSource_m >> ..\%_FILE%
REM git log  --pretty=format:"%%B" --name-only --grep=LODU --since==%_FROM% >> ..\%_FILE%
REM cd ..

rem Prepare
CALL _configureSettings.bat
rem ----

REM for /D %%i in (TerrainsWW2_Jegor) do (
REM for /D %%i in (TerrainsWW2_Rylan) do (
REM for /D %%i in (TerrainsWW2_iron_excelsior) do (
for /D %%i in (TerrainsWW2_swurvin) do (
	cd %%i

	for /D %%j in (*) do (

		"%_APPPATH%\Makepbo.exe" %_PARAMS_STANDARD% "%_SOURCEPATH%\%%i\%%j" "%_TARGETPATH_DEV%\addons\WW2_%%i_%%j" 1>>"%_SOURCEPATH%\BuildPBOs_%%i.log" 2>>&1

	)

	cd..

)

pause

FOR /D %%i IN (Assets_c,Assets_m_SourceIF,Objects_c,Terrains*) DO (

	cd %%i
	git prune
	git gc
	cd ..

)

timeout /t 1
set _FROM=2020-06-07
set _FILE=WW2_ChangeLog_%_FROM%.txt

echo "START" >> %_FILE%

for /D %%i in (Assets_c,Assets_m_SourceIF,Objects_c,Terrains*) DO (

	cd %%i
	echo //%%i >> ..\%_FILE%
	git log --pretty=format:"%%B" --since==%_FROM% >> ..\%_FILE%
	cd ..

)

timeout /t 1

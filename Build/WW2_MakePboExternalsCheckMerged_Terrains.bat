rem Prepare
CALL _configureSettings.bat
rem ----

echo start > "%LOGPATH%\WW2_MakePboExternalsCheck_Merged_Terrains.log"


cd /D "%SOURCEPATH_WW2%"

rem ----

for  /D %%i in (Terrains*) do (

	cd %%i

	for /D %%j in (*) do (

		echo %%A>>"%LOGPATH%\WW2_MakePboExternalsCheck_Merged_Terrains.log"

		"%APPPATH%\MakePbo.exe" %MAKEPBOEXTERNALSCHECK% "%SOURCEPATH_WW2%\%%i\%%j" %TEMPPATH%\temp.pbo 1>>"%LOGPATH%\WW2_MakePboExternalsCheck_Merged_Terrains.log"  2>>&1

	)

	cd..

)

exit